<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerDepanOrtu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('OrtuModel');
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {       
        $username = $this->session->session_login['username'];

        $data_ortu = $this->db->get_where("orang_tua", array("username" => $username))->row();
        $cek_jmlh_balita = $this->db->query("SELECT COUNT(*) as jml_bayi FROM ortu_bayi JOIN balita ON ortu_bayi.nib=balita.nib WHERE id_orang_tua='$data_ortu->id_orang_tua'")->row();
        if ($cek_jmlh_balita->jml_bayi > 1) {
            $data['balita']     = $this->OrtuModel->ambilDataBayi($username);
            $data['jml_balita'] = $cek_jmlh_balita->jml_bayi;
        } else {
            $data['balita'] = $this->OrtuModel->ambilDataBayi($username);
            $data['jml_balita'] = 1;
        }
        
        if(empty($data['balita'])) {
            $this->session->set_flashdata("gagal", "Silahkan hubungi admin untuk koneksi data balita.");
            redirect(site_url('controllerDashboard'));
        }

        $this->load->view('header');
        $this->load->view('v_menuOrtu', $data);
        $this->load->view('footer');
    }

    public function session_filter_bayi()
    {
        $nib = $this->input->post("nib");
        $this->session->set_userdata("nib", $nib);

        echo json_encode(array("status" => true));
    }
}

