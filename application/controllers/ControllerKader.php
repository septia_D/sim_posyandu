<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerKader extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->database();
        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

	public function index()
	{
		$this->load->view('header');
		$this->load->view('v_menuKader');
		$this->load->view('footer');
	}
}
