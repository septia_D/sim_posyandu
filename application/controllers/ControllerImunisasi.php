<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerImunisasi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('ImunisasiModel');
        $this->load->library('form_validation');

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['imunisasi'] = $this->ImunisasiModel->get_all();
        $this->load->view('header');
        $this->load->view('imunisasi/listImunisasi', $data);
        $this->load->view('footer');
    }

    public function insert()
    {
        $data = [
            'button'              => 'Tambah',
            'action'              => site_url("ControllerImunisasi/insert_action"),
            'id_jenis_imunisasi'  => set_value("id_jenis_imunisasi"),
            'nama_imunisasi'      => set_value("nama_imunisasi")
        ];

        $this->load->view('header');
        $this->load->view('imunisasi/formImunisasi', $data);
        $this->load->view('footer');
    }

    public function insert_action()
    {
        $nama_imunisasi = $this->input->post("nama_imunisasi");

        $data = [
            'nama_imunisasi' => $nama_imunisasi,
        ];

        $this->ImunisasiModel->insert($data);
        $this->session->set_flashdata("sukses", "Berhasil tambah data imunisasi.");
        redirect(site_url("controllerImunisasi"));
    }

    public function edit($kode)
    {
        $data = $this->ImunisasiModel->get_by_id($kode);
        if ($data) {
            $data = [
                'button'                => 'Edit',
                'action'                => site_url("controllerImunisasi/edit_action"),
                'id_jenis_imunisasi'    => set_value("id_jenis_imunisasi", $data->id_jenis_imunisasi),
                'nama_imunisasi'        => set_value("nama_imunisasi", $data->nama_imunisasi),
            ];
            $this->load->view("header");
            $this->load->view('imunisasi/formImunisasi', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data jenis imunisasi.");
            redirect(site_url("controllerImunisasi"));
        }
    }

    public function edit_action()
    {
        $id_jenis_imunisasi  = $this->input->post("id_jenis_imunisasi");
        $nama_imunisasi      = $this->input->post("nama_imunisasi");

        $data = [
            'nama_imunisasi' => $nama_imunisasi,
        ];

        $this->ImunisasiModel->update($id_jenis_imunisasi, $data);
        $this->session->set_flashdata("sukses", "Berhasil edit data imunisasi.");

        redirect(site_url("controllerImunisasi"));
    }

    public function delete($kode)
    {
        $data = $this->ImunisasiModel->get_by_id($kode);
        if ($data) {
            $this->ImunisasiModel->delete($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data imunisasi.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data imunisasi.");
        }
        redirect(site_url("controllerImunisasi"));
    }
}
