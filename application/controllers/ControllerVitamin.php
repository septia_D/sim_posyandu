<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerVitamin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('VitaminModel');
        $this->load->library('form_validation');

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['vitamin'] = $this->VitaminModel->get_all();
        $this->load->view('header');
        $this->load->view('vitamin/listVitamin', $data);
        $this->load->view('footer');
    }

    public function insert()
    {
        $data = [
            'button'             => 'Tambah',
            'action'             => site_url("ControllerVitamin/insert_action"),
            'id_jenis_vitamin'   => set_value("id_jenis_vitamin"),
            'nama_vitamin'       => set_value("nama_vitamin")
        ];

        $this->load->view('header');
        $this->load->view('vitamin/formVitamin', $data);
        $this->load->view('footer');
    }

    public function insert_action()
    {
        $nama_vitamin = $this->input->post("nama_vitamin");

        $data = [
            'nama_vitamin' => $nama_vitamin,
        ];

        $this->VitaminModel->insert($data);
        $this->session->set_flashdata("sukses", "Berhasil tambah data vitamin.");
        redirect(site_url("ControllerVitamin"));
    }

    public function edit($kode)
    {
        $data = $this->VitaminModel->get_by_id($kode);
        if ($data) {
            $data = [
                'button'              => 'Edit',
                'action'              => site_url("ControllerVitamin/edit_action"),
                'id_jenis_vitamin'    => set_value("id_jenis_vitamin", $data->id_jenis_vitamin),
                'nama_vitamin'        => set_value("nama_vitamin", $data->nama_vitamin),
            ];
            $this->load->view("header");
            $this->load->view('vitamin/formVitamin', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data jenis vitamin.");
            redirect(site_url("ControllerVitamin"));
        }
    }

    public function edit_action()
    {
        $id_jenis_vitamin  = $this->input->post("id_jenis_vitamin");
        $nama_vitamin      = $this->input->post("nama_vitamin");

        $data = [
            'nama_vitamin' => $nama_vitamin,
        ];

        $this->VitaminModel->update($id_jenis_vitamin, $data);
        $this->session->set_flashdata("sukses", "Berhasil edit data vitamin.");

        redirect(site_url("ControllerVitamin"));
    }

    public function delete($kode)
    {
        $data = $this->VitaminModel->get_by_id($kode);
        if ($data) {
            $this->VitaminModel->delete($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data vitamin.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data vitamin.");
        }
        redirect(site_url("ControllerVitamin"));
    }
}
