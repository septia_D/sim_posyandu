<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerLaporan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model(['VitaminModel', 'ImunisasiModel', 'BalitaModel']);

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $this->load->view('header');
        $this->load->view('v_laporan');
        $this->load->view('footer');
    }

    public function cetak_balita()
    {
        $a = new reportProduct();
        
        $a->AliasNbPages();
        // Potrait ukuran A4
        $a->setKriteria("transaksi");
        
        $a->AddPage("L  ", "A4");
        $a->setNama("DATA BALITA");
        
        $a->SetFont('Arial', '', 8);
        // set lebar tiap kolom tabel transaksi
        $a->SetWidths(array(7, 15, 50, 15, 20, 20, 35, 35, 35, 35));
        $a->Cell(280, 7, 'DATA BALITA', 0, 1, 'C');
        $a->Cell(280, 7, 'POSYANDU CERIA (Mardi Putra 5)', 0, 1, 'C');
        $a->Ln(10);

        // set align tiap kolom tabel transaksi
        $a->SetAligns(array("C", "L", "L", "C", "C", "C", "L", "L", "L"));
        $a->SetFont('Arial', 'B', 7);
        $a->Ln(2);
        // set nama header tabel transaksi
        $a->Cell(7, 7, 'No.', 1, 0, 'C');
        $a->Cell(15, 7, 'NIB', 1, 0, 'C');
        $a->Cell(50, 7, 'NAMA BALITA', 1, 0, 'C');
        $a->Cell(15, 7, 'KELAMIN', 1, 0, 'C');  
        $a->Cell(20, 7, 'NAMA IBU', 1, 0, 'C');
        // $a->Cell(20, 7, 'NAMA AYAH', 1, 0, 'C');
        $a->Cell(20, 7, 'TGL. LAHIR', 1, 0, 'C');
        $a->Cell(35, 7, 'ALAMAT', 1, 0, 'C');
        $a->Cell(35, 7, 'NIK', 1, 0, 'C');
        $a->Cell(35, 7, 'NO. KK', 1, 0, 'C');
        $a->Cell(35, 7, 'KETERANGAN', 1, 0, 'C');
        $a->Ln(7);

        $a->SetFont('Arial', '', 8);

        $data = $this->db->get('balita')->result();

        $n = 0;
        foreach ($data as $r) {
            if($r->is_meninggal == TRUE){
                $ket_meninggal = 'Meninggal                          ';
            } else {
                $ket_meninggal = '';
            }   

            $n++;
            $a->Row(array(($n. "."), $r->nib, $r->nama_balita, $r->jenis_kelamin, $r->nama_ibu, date("d-m-Y", strtotime($r->tgl_lahir)), $r->alamat, $r->nik_balita, $r->no_kk,$ket_meninggal ."Panjang Badan : ". $r->panjang_badan. "  Berat Lahir : ". $r->berat_lahir. "        Lingkar Kepala : ". $r->lingkar_kepala));
            // $a->Ln(5);
        }

        $a->Output();
    }


    public function cetak_pemeriksaan($nib)
    {

        $dt_pemeriksaan = $this->BalitaModel->get_by_id($nib);
        $a = new reportProduct();
        $a->AliasNbPages();
        // Potrait ukuran A4
        $a->setKriteria("transaksi");
        
        $a->AddPage("L  ", "A4");
        $a->setNama("DATA PEMERIKSAAN ". $dt_pemeriksaan->nama_balita);
        
        $a->SetFont('Arial', '', 8);
        // set lebar tiap kolom tabel transaksi
        $a->SetWidths(array(7, 15, 45, 25, 25, 25, 25, 35, 35, 40));
        $a->Cell(280, 7, 'DATA PEMERIKSAAN '. strtoupper($dt_pemeriksaan->nama_balita). " - ". $dt_pemeriksaan->nib, 0, 1, 'C');
        $a->Cell(280, 7, 'POSYANDU CERIA (Mardi Putra 5)', 0, 1, 'C');
        $a->Ln(10);

        // set align tiap kolom tabel transaksi
        $a->SetAligns(array("C", "L", "L", "C", "C", "C", "C", "L", "L"));
        $a->SetFont('Arial', 'B', 7);
        $a->Ln(2);
        // set nama header tabel transaksi
        $a->Cell(7, 7, 'No.', 1, 0, 'C');
        $a->Cell(15, 7, 'NIB', 1, 0, 'C');
        $a->Cell(45, 7, 'NAMA BALITA', 1, 0, 'C');
        $a->Cell(25, 7, 'TGL. TIMBANG', 1, 0, 'C');  
        $a->Cell(25, 7, 'BERAT BADAN', 1, 0, 'C');
        $a->Cell(25, 7, 'PANJANG BADAN', 1, 0, 'C');
        $a->Cell(25, 7, 'LINGKAR PERUT', 1, 0, 'C');
        $a->Cell(35, 7, 'JENIS IMUNISASI', 1, 0, 'C');
        $a->Cell(35, 7, 'JENIS VITAMIN', 1, 0, 'C');
        $a->Cell(40, 7, 'SARAN', 1, 0, 'C');
        $a->Ln(7);

        $a->SetFont('Arial', '', 8);

        $data = $this->db->query("SELECT * FROM balita JOIN pemeriksaan ON balita.nib=pemeriksaan.nib JOIN jenis_imunisasi ON pemeriksaan.id_jenis_imunisasi=jenis_imunisasi.id_jenis_imunisasi JOIN jenis_vitamin ON pemeriksaan.id_jenis_vitamin=jenis_vitamin.id_jenis_vitamin")->result();

        $n = 0;
        foreach ($data as $r) {
            $n++;
            $a->Row(array(($n. "."), $r->nib, $r->nama_balita. " / Umur : ".$r->umur, date("d-m-Y", strtotime($r->tgl_timbang)), $r->berat_badan.  " kg", $r->panjang_badan.  " cm", $r->lingkar_perut.  " cm", $r->nama_imunisasi, $r->nama_vitamin, $r->saran));
            // $a->Ln(5);
        }

        $a->Output();
    }

    public function cetak_kematian()
    {
        $a = new reportProduct();
        
        $a->AliasNbPages();
        // Potrait ukuran A4
        $a->setKriteria("transaksi");
        
        $a->AddPage("L  ", "A4");
        $a->setNama("DATA KEMATIAN BALITA");
        
        $a->SetFont('Arial', '', 8);
        // set lebar tiap kolom tabel transaksi
        $a->SetWidths(array(7, 15, 50, 15, 20, 20, 35, 35, 35, 40));
        $a->Cell(280, 7, 'DATA KEMATIAN BALITA', 0, 1, 'C');
        $a->Cell(280, 7, 'POSYANDU CERIA (Mardi Putra 5)', 0, 1, 'C');
        $a->Ln(10);

        // set align tiap kolom tabel transaksi
        $a->SetAligns(array("C", "L", "L", "C", "C", "C", "L", "L", "L"));
        $a->SetFont('Arial', 'B', 7);
        $a->Ln(2);
        // set nama header tabel transaksi
        $a->Cell(7, 7, 'No.', 1, 0, 'C');
        $a->Cell(15, 7, 'NIB', 1, 0, 'C');
        $a->Cell(50, 7, 'NAMA BALITA', 1, 0, 'C');
        $a->Cell(15, 7, 'KELAMIN', 1, 0, 'C');  
        $a->Cell(20, 7, 'NAMA IBU', 1, 0, 'C');
        $a->Cell(20, 7, 'TGL. LAHIR', 1, 0, 'C');
        $a->Cell(35, 7, 'ALAMAT', 1, 0, 'C');
        $a->Cell(35, 7, 'NIK', 1, 0, 'C');
        $a->Cell(35, 7, 'NO. KK', 1, 0, 'C');
        $a->Cell(40, 7, 'KETERANGAN', 1, 0, 'C');
        $a->Ln(7);

        $a->SetFont('Arial', '', 8);

        $data = $this->db->query("SELECT * FROM balita JOIN kematian ON balita.nib=kematian.nib")->result();

        $n = 0;
        foreach ($data as $r) {
            $n++;
            $a->Row(array(($n. "."), $r->nib, $r->nama_balita, $r->jenis_kelamin, $r->nama_ibu, date("d-m-Y", strtotime($r->tgl_lahir)), $r->alamat, $r->nik_balita, $r->no_kk, "Tgl. Kematian : ". date("d-m-Y", strtotime($r->tgl_kematian)). "  Keterangan : ". $r->keterangan));
            // $a->Ln(5);
        }

        $a->Output();
    }
}
