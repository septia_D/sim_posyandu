<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerJadwal extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(['VitaminModel', 'JadwalModel']);
        $this->load->library('form_validation');

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['jadwal'] = $this->JadwalModel->get_all();
        $this->load->view('header');
        $this->load->view('jadwal/listJadwal', $data);
        $this->load->view('footer');
    }

    public function insert()
    {
        $data = [
            'button'                => 'Tambah',
            'action'                => site_url("controllerJadwal/insert_action"),
            'id_jadwal_pemeriksaan' => set_value("id_jadwal_pemeriksaan"),
            'tgl_jadwal'            => set_value("tgl_jadwal"),
            'jam_mulai'             => set_value("jam_mulai"),
            'jam_selese'            => set_value("jam_selese"),
        ];

        $this->load->view('header');
        $this->load->view('jadwal/formJadwal', $data);
        $this->load->view('footer');
    }

    public function insert_action()
    {
        $id_jadwal_pemeriksaan  = $this->input->post("id_jadwal_pemeriksaan");
        $tgl_jadwal             = date('Y-m-d', strtotime($this->input->post("tgl_jadwal")));
        $jam_mulai              = $this->input->post("jam_mulai");
        $jam_selese             = $this->input->post("jam_selese");

        $data = [
            'id_jadwal_pemeriksaan' => $id_jadwal_pemeriksaan,
            'tgl_jadwal'            => $tgl_jadwal,
            'jam_mulai'             => $jam_mulai,
            'jam_selese'            => $jam_selese,
        ];

        $this->JadwalModel->insert($data);
        $this->session->set_flashdata("sukses", "Berhasil tambah data jadwal.");
        redirect(site_url("ControllerJadwal"));
    }

    public function edit($kode)
    {
        $data = $this->JadwalModel->get_by_id($kode);
        if ($data) {
            $data = [
                'button'                => 'Edit',
                'action'                => site_url("ControllerJadwal/edit_action"),
                'id_jadwal_pemeriksaan' => set_value("id_jadwal_pemeriksaan", $data->id_jadwal_pemeriksaan),
                'tgl_jadwal'            => set_value("tgl_jadwal", $data->tgl_jadwal),
                'jam_mulai'             => set_value("jam_mulai", $data->jam_mulai),
                'jam_selese'            => set_value("jam_selese", $data->jam_selese),
            ];
            $this->load->view("header");
            $this->load->view('jadwal/formJadwal', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data jadwal.");
            redirect(site_url("ControllerJadwal"));
        }
    }

    public function edit_action()
    {
        $id_jadwal_pemeriksaan  = $this->input->post("id_jadwal_pemeriksaan");
        $tgl_jadwal             = date('Y-m-d', strtotime($this->input->post("tgl_jadwal")));
        $jam_mulai              = $this->input->post("jam_mulai");
        $jam_selese             = $this->input->post("jam_selese");

        $data = [
            'tgl_jadwal'            => $tgl_jadwal,
            'jam_mulai'             => $jam_mulai,
            'jam_selese'            => $jam_selese,
        ];

        $this->JadwalModel->update($id_jadwal_pemeriksaan, $data);
        $this->session->set_flashdata("sukses", "Berhasil edit data jadwal.");

        redirect(site_url("ControllerJadwal"));
    }

    public function delete($kode)
    {
        $data = $this->JadwalModel->get_by_id($kode);
        if ($data) {
            $this->JadwalModel->delete($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data jadwal.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data jadwal.");
        }
        redirect(site_url("ControllerJadwal"));
    }

    public function lihat_jadwal()
    {
        $data['jadwal'] = $this->JadwalModel->get_all();
        $this->load->view("header");
        $this->load->view('jadwal/lihatJadwal', $data);
        $this->load->view("footer");
    }

    public function get_data_jadwal()
    {
        // Our Start and End Dates
        $start      = $this->input->get("start");
        $end        = $this->input->get("end");

        $startdt = new DateTime('now'); // setup a local datetime
        $startdt->setTimestamp($start); // Set the date based on timestamp
        $start_format = $startdt->format('Y-m-d H:i:s');

        $enddt = new DateTime('now'); // setup a local datetime
        $enddt->setTimestamp($end); // Set the date based on timestamp
        $end_format = $enddt->format('Y-m-d H:i:s');

        $events = $this->JadwalModel->get_all($start_format, $end_format);

        $data_events = array();

        foreach ($events as $r) {

            $data_events[] = array(
                "title" => "\n ". $r["jam_mulai"]. "\n - \n". $r["jam_selese"],
                "start_event" => $r["tgl_jadwal"],
            );
        }

        echo json_encode(array("events" => $data_events));
        exit();
    }
}
