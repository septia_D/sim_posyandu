<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerBalita extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(['VitaminModel', 'ImunisasiModel', 'BalitaModel']);
        $this->load->library('form_validation');

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['balita'] = $this->BalitaModel->get_all();
        $this->load->view('header');
        $this->load->view('balita/listBalita', $data);
        $this->load->view('footer');
    }

    public function insert()
    {
        $data = [
            'button'             => 'Tambah',
            'action'             => site_url("ControllerBalita/insert_action"),
            'nib'                => $this->BalitaModel->nib(),
            'nama_balita'        => set_value("nama_balita"),
            'tgl_lahir'          => set_value("tgl_lahir"),
            'jenis_kelamin'      => set_value("jenis_kelamin"),
            'nama_ibu'           => set_value("nama_ibu"),
            'nama_ayah'          => set_value("nama_ayah"),
            'alamat'             => set_value("alamat"),
            'panjang_badan'      => set_value("panjang_badan"),
            'berat_lahir'        => set_value("berat_lahir"),
            'lingkar_kepala'     => set_value("lingkar_kepala"),
            'nik_balita'         => set_value("nik_balita"),
            'no_kk'              => set_value("no_kk"),
            'anak_ke'            => set_value("anak_ke"),
        ];

        $this->load->view('header');
        $this->load->view('balita/formBalita', $data);
        $this->load->view('footer');
    }

    public function insert_action()
    {
        $nib            = $this->input->post("nib");
        $nama_balita    = $this->input->post("nama_balita");
        $tgl_lahir      = date('Y-m-d', strtotime($this->input->post("tgl_lahir")));
        $jenis_kelamin  = $this->input->post("jenis_kelamin");
        $nama_ibu       = $this->input->post("nama_ibu");
        $nama_ayah      = $this->input->post("nama_ayah");
        $alamat         = $this->input->post("alamat");
        $panjang_badan  = $this->input->post("panjang_badan");
        $berat_lahir    = $this->input->post("berat_lahir");
        $lingkar_kepala = $this->input->post("lingkar_kepala");
        $nik_balita     = $this->input->post("nik_balita");
        $no_kk          = $this->input->post("no_kk");
        $anak_ke        = $this->input->post("anak_ke");

        $data = [
            'nib'            => $nib,
            'nama_balita'    => $nama_balita,
            'tgl_lahir'      => $tgl_lahir,
            'jenis_kelamin'  => $jenis_kelamin,
            'nama_ibu'       => $nama_ibu,
            'nama_ayah'      => $nama_ayah,
            'alamat'         => $alamat,
            'panjang_badan'  => $panjang_badan,
            'berat_lahir'    => $berat_lahir,
            'lingkar_kepala' => $lingkar_kepala,
            'nik_balita'     => $nik_balita,
            'no_kk'          => $no_kk,
            'anak_ke'        => $anak_ke,
        ];

        $this->BalitaModel->insert($data);
        $this->session->set_flashdata("sukses", "Berhasil tambah data balita.");
        redirect(site_url("ControllerBalita"));
    }

    public function edit($kode)
    {
        $data = $this->BalitaModel->get_by_id($kode);
        if ($data) {
            $data = [
                'button'              => 'Edit',
                'action'              => site_url("ControllerBalita/edit_action"),
                'nib'                 => set_value("nib", $data->nib),
                'nama_balita'         => set_value("nama_balita", $data->nama_balita),
                'tgl_lahir'           => set_value("tgl_lahir", $data->tgl_lahir),
                'jenis_kelamin'       => set_value("jenis_kelamin", $data->jenis_kelamin),
                'nama_ibu'            => set_value("nama_ibu", $data->nama_ibu),
                'nama_ayah'           => set_value("nama_ayah", $data->nama_ayah),
                'alamat'              => set_value("alamat", $data->alamat),
                'panjang_badan'       => set_value("panjang_badan", $data->panjang_badan),
                'berat_lahir'         => set_value("berat_lahir", $data->berat_lahir),
                'lingkar_kepala'      => set_value("lingkar_kepala", $data->lingkar_kepala),
                'nik_balita'          => set_value("nik_balita", $data->nik_balita),
                'no_kk'               => set_value("no_kk", $data->no_kk),
                'anak_ke'             => set_value("anak_ke", $data->anak_ke),
            ];
            $this->load->view("header");
            $this->load->view('balita/formBalita', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data balita.");
            redirect(site_url("ControllerBalita"));
        }
    }

    public function edit_action()
    {
        $nib            = $this->input->post("nib");
        $nama_balita    = $this->input->post("nama_balita");
        $tgl_lahir      = date('Y-m-d', strtotime($this->input->post("tgl_lahir")));
        $jenis_kelamin  = $this->input->post("jenis_kelamin");
        $nama_ibu       = $this->input->post("nama_ibu");
        $nama_ayah      = $this->input->post("nama_ayah");
        $alamat         = $this->input->post("alamat");
        $panjang_badan  = $this->input->post("panjang_badan");
        $berat_lahir    = $this->input->post("berat_lahir");
        $lingkar_kepala = $this->input->post("lingkar_kepala");
        $nik_balita     = $this->input->post("nik_balita");
        $no_kk          = $this->input->post("no_kk");
        $anak_ke        = $this->input->post("anak_ke");

        $data = [
            'nama_balita'    => $nama_balita,
            'tgl_lahir'      => $tgl_lahir,
            'jenis_kelamin'  => $jenis_kelamin,
            'nama_ibu'       => $nama_ibu,
            'nama_ayah'      => $nama_ayah,
            'alamat'         => $alamat,
            'panjang_badan'  => $panjang_badan,
            'berat_lahir'    => $berat_lahir,
            'lingkar_kepala' => $lingkar_kepala,
            'nik_balita'     => $nik_balita,
            'no_kk'          => $no_kk,
            'anak_ke'        => $anak_ke,
        ];

        $this->BalitaModel->update($nib, $data);
        $this->session->set_flashdata("sukses", "Berhasil edit data balita.");

        redirect(site_url("ControllerBalita"));
    }

    public function delete($kode)
    {
        $data = $this->BalitaModel->get_by_id($kode);
        if ($data) {
            $this->BalitaModel->delete($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data balita.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data balita.");
        }
        redirect(site_url("ControllerBalita"));
    }

    public function view($kode)
    {
        $data['balita']      = $this->BalitaModel->get_by_id($kode);
        $data['pemeriksaan'] = $this->BalitaModel->get_pemeriksaan_all($kode);
        $this->load->view('header');
        $this->load->view('balita/viewBalita', $data);
        $this->load->view('footer');
    }

    public function insert_pemeriksaan()
    {
        $nib                = $this->input->post("nib");
        $tgl_timbang        = date('Y-m-d', strtotime($this->input->post("tgl_timbang")));
        $berat_badan        = $this->input->post("berat_badan");
        $panjang_badan      = $this->input->post("panjang_badan");
        $lingkar_perut      = $this->input->post("lingkar_perut");
        $id_jenis_imunisasi = $this->input->post("id_jenis_imunisasi");
        $id_jenis_vitamin   = $this->input->post("id_jenis_vitamin");
        $saran              = $this->input->post("saran");
        $umur               = $this->input->post("umur");

        $data = [
            'kode_pemeriksaan'      => $this->BalitaModel->kode_pemeriksaan(),
            'nib'                   => $nib,
            'tgl_timbang'           => $tgl_timbang,
            'berat_badan'           => $berat_badan,
            'panjang_badan'         => $panjang_badan,
            'lingkar_perut'         => $lingkar_perut,
            'id_jenis_imunisasi'    => $id_jenis_imunisasi,
            'id_jenis_vitamin'      => $id_jenis_vitamin,
            'saran'                 => $saran,
            'umur'                  => $umur,
        ];

        $this->BalitaModel->insert_pemeriksaan($data);
        $this->session->set_flashdata("sukses", "Berhasil tambah data pemeriksaan.");
        redirect(site_url("controllerBalita/view/" . $nib));
    }

    public function edit_pemeriksaan($kode)
    {
        $data = $this->BalitaModel->get_by_id_pemeriksaan($kode);
        if ($data) {
            $data = [
                'button'             => 'Edit',
                'action'             => site_url("ControllerBalita/edit_pemeriksaan_action"),
                'kode_pemeriksaan'   => set_value("kode_pemeriksaan", $data->kode_pemeriksaan),
                'nib'                => set_value("nib ", $data->nib),
                'tgl_timbang'        => set_value("tgl_timbang", $data->tgl_timbang),
                'berat_badan'        => set_value("berat_badan", $data->berat_badan),
                'panjang_badan'      => set_value("panjang_badan", $data->panjang_badan),
                'lingkar_perut'      => set_value("lingkar_perut", $data->lingkar_perut),
                'id_jenis_imunisasi' => set_value("id_jenis_imunisasi", $data->id_jenis_imunisasi),
                'id_jenis_vitamin'   => set_value("id_jenis_vitamin", $data->id_jenis_vitamin),
                'saran'              => set_value("saran", $data->saran),
            ];
            $this->load->view("header");
            $this->load->view('balita/formBalitaPemeriksaan', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data pemeriksaan.");
            redirect(site_url("controllerBalita/view/" . $$data->nib));

        }
    }

    public function edit_pemeriksaan_action()
    {
        $kode_pemeriksaan   = $this->input->post("kode_pemeriksaan");
        $nib                = $this->input->post("nib");
        $tgl_timbang        = date('Y-m-d', strtotime($this->input->post("tgl_timbang")));
        $berat_badan        = $this->input->post("berat_badan");
        $panjang_badan      = $this->input->post("panjang_badan");
        $lingkar_perut      = $this->input->post("lingkar_perut");
        $id_jenis_imunisasi = $this->input->post("id_jenis_imunisasi");
        $id_jenis_vitamin   = $this->input->post("id_jenis_vitamin");
        $saran              = $this->input->post("saran");

        $data = [
            'nib'                   => $nib,
            'tgl_timbang'           => $tgl_timbang,
            'berat_badan'           => $berat_badan,
            'panjang_badan'         => $panjang_badan,
            'lingkar_perut'         => $lingkar_perut,
            'id_jenis_imunisasi'    => $id_jenis_imunisasi,
            'id_jenis_vitamin'      => $id_jenis_vitamin,
            'saran'                 => $saran,
        ];

        $this->BalitaModel->update_pemeriksaan($kode_pemeriksaan, $data);
        $this->session->set_flashdata("sukses", "Berhasil edit data pemeriksaan.");
        redirect(site_url("controllerBalita/view/" . $nib));
    }

    public function delete_pemeriksaan($kode, $nib)
    {
        $data = $this->BalitaModel->get_by_id_pemeriksaan($kode);
        if ($data) {
            $this->BalitaModel->delete_pemeriksaan($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data pemeriksaan.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data pemeriksaan.");
        }
        redirect(site_url("controllerBalita/view/" . $nib));
    }
}
