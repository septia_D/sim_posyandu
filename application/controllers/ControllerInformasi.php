<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerInformasi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('InformasiModel');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url', 'download', 'file'));

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['informasi'] = $this->InformasiModel->get_all();
        $this->load->view('header');
        $this->load->view('informasi/listInformasi', $data);
        $this->load->view('footer');
    }

    public function insert()
    {
        $data = [
            'button'        => 'Tambah',
            'action'        => site_url("controllerInformasi/insert_action"),
            'id_informasi'  => set_value("id_informasi"),
            'judul'         => set_value("judul"),
            'isi'           => set_value("isi"),
            'tgl_post'      => set_value("tgl_post"),
            'foto'          => set_value("foto"),
        ];

        $this->load->view('header');
        $this->load->view('informasi/formInformasi', $data);
        $this->load->view('footer');
    }

    public function insert_action()
    {
        $id_informasi   = $this->input->post("id_informasi");
        $judul          = $this->input->post("judul");
        $isi            = $this->input->post("isi");
        $tgl_post       = date('Y-m-d H:i:s', strtotime($this->input->post("tgl_post")));

        // konfigurasi untuk melakukan upload photo
        $config['upload_path']   = './images/informasi';    //path folder image
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
        $config['file_name']     = $this->input->post('judul'); //nama file photo dirubah menjadi nama berdasarkan nim	
        $this->upload->initialize($config);

        if (!empty($_FILES['foto']['name'])) {
            if ($this->upload->do_upload('foto')) {
                $photo = $this->upload->data();
                $dataphoto = $photo['file_name'];
                $this->load->library('upload', $config);

                $data = [
                    'id_informasi'  => $id_informasi,
                    'judul'         => $judul,
                    'isi'           => $isi,
                    'tgl_post'      => $tgl_post,
                    'foto'          => $dataphoto,
                ];

                $this->InformasiModel->insert($data);
            }
        } else {

            $data = [
                'id_informasi'  => $id_informasi,
                'judul'         => $judul,
                'isi'           => $isi,
                'tgl_post'      => $tgl_post,
            ];

            $this->InformasiModel->insert($data);
        }

        $this->session->set_flashdata("sukses", "Berhasil tambah data informasi.");
        redirect(site_url("controllerInformasi"));
    }

    public function edit($kode)
    {
        $data = $this->InformasiModel->get_by_id($kode);
        if ($data) {
            $data = [
                'button'            => 'Edit',
                'action'            => site_url("controllerInformasi/edit_action"),
                'id_informasi'      => set_value("id_informasi", $data->id_informasi),
                'judul'             => set_value("judul", $data->judul),
                'isi'               => set_value("isi", $data->isi),
                'tgl_post'          => set_value("tgl_post", $data->tgl_post),
                'foto'              => set_value("foto", $data->foto),
            ];
            $this->load->view("header");
            $this->load->view('informasi/formInformasi', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data informasi.");
            redirect(site_url("controllerInformasi"));
        }
    }

    public function edit_action()
    {
        $id_informasi   = $this->input->post("id_informasi");
        $judul          = $this->input->post("judul");
        $isi            = $this->input->post("isi");
        $tgl_post       = date('Y-m-d H:i:s', strtotime($this->input->post("tgl_post")));

        // konfigurasi untuk melakukan upload photo
        $config['upload_path']   = './images/informasi';    //path folder image
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
        $config['file_name']     = $this->input->post('judul'); //nama file photo dirubah menjadi nama berdasarkan nim	
        $this->upload->initialize($config);

        if (!empty($_FILES['foto']['name'])) {
            if ($this->upload->do_upload('foto')) {
                $photo = $this->upload->data();
                $dataphoto = $photo['file_name'];
                $this->load->library('upload', $config);

                $data = [
                    'judul'         => $judul,
                    'isi'           => $isi,
                    'tgl_post'      => $tgl_post,
                    'foto'          => $dataphoto,
                ];

                $this->InformasiModel->update($id_informasi, $data);
            }
        } else {

            $data = [
                'judul'         => $judul,
                'isi'           => $isi,
                'tgl_post'      => $tgl_post,
            ];

            $this->InformasiModel->update($id_informasi, $data);
        }
        $this->session->set_flashdata("sukses", "Berhasil edit data informasi.");

        redirect(site_url("controllerInformasi"));
    }

    public function delete($kode)
    {
        $data = $this->InformasiModel->get_by_id($kode);
        if ($data) {
            $this->InformasiModel->delete($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data informasi.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data informasi.");
        }
        redirect(site_url("controllerInformasi"));
    }
}
