<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerDashboard extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('InformasiModel');

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

	public function index()
	{
        $data['informasi'] = $this->InformasiModel->get_all();

		$this->load->view('header');
		$this->load->view('dashboard', $data);
		$this->load->view('footer');
	}
}
