<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerOrtu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(['OrtuModel', 'UserModel']);
        $this->load->library('form_validation');

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['ortu'] = $this->OrtuModel->get_all();
        $this->load->view('header');
        $this->load->view('ortu/listOrtu', $data);
        $this->load->view('footer');
    }

    public function insert()
    {
        $data = [
            'button'        => 'Tambah',
            'action'        => site_url("controllerOrtu/insert_action"),
            'id_orang_tua'  => set_value("id_orang_tua"),
            'nama'          => set_value("nama"),
            'username'      => set_value("username"),
        ];

        $this->load->view('header');
        $this->load->view('ortu/formOrtu', $data);
        $this->load->view('footer');
    }

    public function insert_action()
    {
        $nama       = $this->input->post("nama");
        $username   = $this->input->post("username");
        $password   = md5($this->input->post("password"));

        $dt_username = $this->db->get_where("orang_tua", array("username" => $username))->row();
        if ($dt_username) {
            $this->session->set_flashdata("gagal", "Username sudah dipakai, cari yang lain.");
            redirect(site_url("controllerOrtu"));
        } else {
            $data = [
                'nama' => $nama,
                'username'  => $username,
            ];

            $data_user = [
                'nama'      => $nama,
                'username'  => $username,
                'password'  => $password,
                'role'      => 'member',
                'aktif'     => '1',
            ];

            $this->OrtuModel->insert($data);
            $this->UserModel->insert($data_user);
            $this->session->set_flashdata("sukses", "Berhasil tambah data orang tua.");
            redirect(site_url("controllerOrtu"));
        }
    }

    public function edit($kode)
    {
        $data = $this->OrtuModel->get_by_id($kode);
        if ($data) {
            $data = [
                'button'        => 'Edit',
                'action'        => site_url("controllerOrtu/edit_action"),
                'id_orang_tua'  => set_value("id_orang_tua", $data->id_orang_tua),
                'nama'          => set_value("nama", $data->nama),
                'username'      => set_value("username", $data->username),
            ];
            $this->load->view("header");
            $this->load->view('ortu/formOrtu', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data orangtua.");
            redirect(site_url("controllerOrtu"));
        }
    }

    public function edit_action()
    {
        $id_orang_tua  = $this->input->post("id_orang_tua");
        $nama          = $this->input->post("nama");
        $username      = $this->input->post("username");

        $data = [
            'nama'      => $nama,
            'username'  => $username,
        ];

        $data_user = [
            'nama'      => $nama,
            'username'  => $username,
        ];

        $dt_ortu_sebelumnya = $this->OrtuModel->get_by_id($id_orang_tua);

        $this->OrtuModel->update($id_orang_tua, $data);
        $this->UserModel->update($dt_ortu_sebelumnya->username, $data_user);
        $this->session->set_flashdata("sukses", "Berhasil edit data orangtua.");

        redirect(site_url("controllerOrtu"));
    }

    public function delete($kode)
    {
        $data = $this->OrtuModel->get_by_id($kode);
        if ($data) {
            $dt_user = $this->db->get_where("user", array("username" => $data->username))->row();
            if ($dt_user) {
                $this->UserModel->delete($data->username);
            }

            $dt_ortu_bayi = $this->OrtuModel->get_ortu_bayi_id($data->id_orang_tua);
            if ($dt_ortu_bayi) {
                $this->OrtuModel->delete_ortu_bayi_id($data->id_orang_tua);
            }

            $this->OrtuModel->delete($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data orang tua.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data orang tua.");
        }
        redirect(site_url("controllerOrtu"));
    }

    public function tambah_bayi_form($id)
    {
        $data['id_ortu'] = $id;
        $this->load->view('header');
        $this->load->view('ortu/formOrtuBayi', $data);
        $this->load->view('footer');
    }

    public function tambah_balita()
    {
        $id_orang_tua  = $this->input->post("id_ortu");
        $nib           = $this->input->post("nib");

        $data = [
            'id_orang_tua'  => $id_orang_tua,
            'nib'           => $nib,
        ];

        $this->OrtuModel->insert_ortu_bayi($data);
        $this->session->set_flashdata("sukses", "Berhasil.");
        redirect(site_url("controllerOrtu"));
    }

    public function hapus_ortu_bayi($kode)
    {
        $data = $this->OrtuModel->get_ortu_bayi($kode);
        if ($data) {
            $this->OrtuModel->delete_ortu_bayi($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus.");
        }
        redirect(site_url("controllerOrtu"));
    }
}
