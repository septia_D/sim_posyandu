<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerLogin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('LoginModel');
    }

    public function index()
    {
        if (empty($this->session->userdata("username"))) {
            $this->load->view("view_login");
        } else {
            redirect("ControllerDashboard");
        }
    }

    // fungsi cek status login untuk menampilkan menu berdasarkan role masing2 user
    public function cekStatusLogin()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_message('required', '* {field} Harus diisi');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $username = $this->input->post("username", TRUE);
            $password = md5($this->input->post("password", TRUE));

            $where = ["username" => $username, "aktif" => '1'];
            $cek = $this->LoginModel->validasi("user", $where)->row_array();
            if ($cek) {
                $username_db = $cek['username'];
                $password_db = $cek['password'];
                if ($username == $username_db && $password == $password_db) {

                    $data_session = [
                        'id_user'  => $cek['id_user'],
                        'username' => $username,
                        'role'     => $cek['role'],
                    ];

                    $this->session->set_userdata("session_login", $data_session);
                    redirect(site_url("controllerDashboard"));
                } else {
                    $this->session->set_flashdata("gagal", "Username atau Password salah.");
                    redirect("controllerLogin");
                }
            } else {
                $this->session->set_flashdata("gagal", "User tidak aktif.");
                redirect("controllerLogin");
            }
        }
    }

    public function ubahPassword()
    {
        $this->load->view('header');
        $this->load->view('ubah_password');
        $this->load->view('footer');
    }

    public function ubah_password_action()
    {
        $username  = $this->session->session_login["username"];
        $password  = md5($this->input->post("password"));

        $data = [
            "password"  => $password
        ];

        $this->LoginModel->updateUser($username, $data);
        $this->session->set_flashdata("sukses", "Berhasil ubah password.");
        redirect("ControllerDashboard");
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect("ControllerLogin");
    }
}
