<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ControllerKematian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(['KematianModel', 'BalitaModel']);
        $this->load->library('form_validation');

        if (empty($this->session->session_login['username'])) {
            $this->session->set_flashdata("gagal", "Anda harus login terlebih dahulu.");
            redirect(site_url("controllerLogin"));
        }
    }

    public function index()
    {
        $data['kematian'] = $this->KematianModel->get_all();
        $this->load->view('header');
        $this->load->view('kematian/listKematian', $data);
        $this->load->view('footer');
    }

    public function insert()
    {
        $data = [
            'button'         => 'Tambah',
            'action'         => site_url("controllerKematian/insert_action"),
            'id_kematian'    => set_value("id_kematian"),
            'nib'            => set_value("nib"),
            'tgl_kematian'   => set_value("tgl_kematian"),
            'keterangan'     => set_value("keterangan"),
        ];

        $this->load->view('header');
        $this->load->view('kematian/formKematian', $data);
        $this->load->view('footer');
    }

    public function insert_action()
    {
        $nib            = $this->input->post("nib");
        $tgl_kematian   = $this->input->post("tgl_kematian");
        $keterangan     = $this->input->post("keterangan");

        $data = [
            'nib'           => $nib,
            'tgl_kematian'  => $tgl_kematian,
            'keterangan'    => $keterangan,
        ];

        $data_balita = [
            'is_meninggal' => TRUE
        ];

        $this->KematianModel->insert($data);
        $this->BalitaModel->update($nib, $data_balita);
        $this->session->set_flashdata("sukses", "Berhasil tambah data kematian.");
        redirect(site_url("controllerKematian"));
    }

    public function edit($kode)
    {
        $data_kematian = $this->KematianModel->get_by_id($kode);
        if ($data_kematian) {
            $data = [
                'button'         => 'Edit',
                'action'         => site_url("controllerKematian/edit_action"),
                'id_kematian'    => set_value("id_kematian", $data_kematian->id_kematian),
                'nib'            => set_value("nib", $data_kematian->nib),
                'tgl_kematian'   => set_value("tgl_kematian", $data_kematian->tgl_kematian),
                'keterangan'     => set_value("keterangan", $data_kematian->keterangan),
            ];
            // print_r($data);die;

            $this->load->view("header");
            $this->load->view('kematian/formKematian', $data);
            $this->load->view("footer");
        } else {
            $this->session->set_flashdata("gagal", "Gagal edit data kematian.");
            redirect(site_url("controllerKematian"));
        }
    }

    public function edit_action()
    {
        $id_kematian    = $this->input->post("id_kematian");
        $tgl_kematian   = $this->input->post("tgl_kematian");
        $keterangan     = $this->input->post("keterangan");

        $data = [
            'tgl_kematian'  => $tgl_kematian,
            'keterangan'    => $keterangan,
        ];

        $this->KematianModel->update($id_kematian, $data);
        $this->session->set_flashdata("sukses", "Berhasil edit data kematian.");

        redirect(site_url("controllerKematian"));
    }

    public function delete($kode)
    {
        $data = $this->KematianModel->get_by_id($kode);
        if ($data) {
            $dt_balita = $this->BalitaModel->get_by_id($data->nib);
            if(!empty($dt_balita)){

                $data_balita = [
                    'is_meninggal' => FALSE
                ];
                
                $this->BalitaModel->update($data->nib, $data_balita);
            }

            $this->KematianModel->delete($kode);
            $this->session->set_flashdata("sukses", "Berhasil hapus data kematian.");
        } else {
            $this->session->set_flashdata("gagal", "Gagal hapus data kematian.");
        }
        redirect(site_url("controllerKematian"));
    }

    function get_dtBalita()
    {
        $nib = $this->input->post("nib");
        $data = $this->BalitaModel->get_by_id($nib);
        $result = [
            'nama_ibu'  => $data->nama_ibu,
            'tgl_lahir' => date('d-m-Y', strtotime($data->tgl_lahir)),
        ];

        echo json_encode($result);
    }
}
