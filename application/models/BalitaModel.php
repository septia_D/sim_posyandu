<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BalitaModel extends CI_Model {
    
    function get_all()
    {
        $this->db->order_by('nib', 'ASC');
        $query = $this->db->get("balita");
        return $query->result_array();
    }

    function insert($data)
    {
        $this->db->insert('balita', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('nib', $kode);
        return $this->db->get("balita")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("nib", $kode);
        $this->db->update("balita", $data);
    }

    function delete($kode)
    {
        $this->db->where("nib", $kode);
        $this->db->delete("balita");
    }

    function nib()
	{
		//kode baru
		$cari 		= "B";
		$nomer 		= "SELECT MAX(cast(SUBSTRING(nib,2,3) as integer)*1)as a FROM balita where SUBSTRING(nib,1,1) like '%$cari%'";
		$baris 		= $this->db->query($nomer);
		$akhir 		= $baris->row()->a;
		$akhir++;
		$nota 		= str_pad($akhir, 3, "0", STR_PAD_LEFT);
		$format 	= "B";
		$format 	.= "$nota";
		return $format;
    }
    
    function kode_pemeriksaan()
    {
        $cari 		= "P";
		$nomer 		= "SELECT MAX(cast(SUBSTRING(kode_pemeriksaan,2,3) as integer)*1)as a FROM pemeriksaan where SUBSTRING(kode_pemeriksaan,1,1) like '%$cari%'";
		$baris 		= $this->db->query($nomer);
		$akhir 		= $baris->row()->a;
		$akhir++;
		$nota 		= str_pad($akhir, 3, "0", STR_PAD_LEFT);
		$format 	= "P";
		$format 	.= "$nota";
		return $format;
    }

    function insert_pemeriksaan($data)
    {
        $this->db->insert('pemeriksaan', $data);
    }
    
    function get_pemeriksaan_all($kode)
    {
        $this->db->select("a.*, b.*, c.*");
        $this->db->from("pemeriksaan as a");
        $this->db->join("jenis_imunisasi as b", "a.id_jenis_imunisasi=b.id_jenis_imunisasi");
        $this->db->join("jenis_vitamin as c", "a.id_jenis_vitamin=c.id_jenis_vitamin");

        $this->db->where("a.nib", $kode);
        $this->db->order_by('kode_pemeriksaan', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_by_id_pemeriksaan($kode)
    {
        $this->db->where('kode_pemeriksaan', $kode);
        return $this->db->get("pemeriksaan")->row();
    }

    function update_pemeriksaan($kode, $data)
    {
        $this->db->where("kode_pemeriksaan", $kode);
        $this->db->update("pemeriksaan", $data);
    }

    function delete_pemeriksaan($kode)
    {
        $this->db->where("kode_pemeriksaan", $kode);
        $this->db->delete("pemeriksaan");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */