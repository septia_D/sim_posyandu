<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImunisasiModel extends CI_Model {
    
    function get_all()
    {
        $query = $this->db->get("jenis_imunisasi");
        return $query->result_array();
    }

    function insert($data)
    {
        $this->db->insert('jenis_imunisasi', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('id_jenis_imunisasi', $kode);
        return $this->db->get("jenis_imunisasi")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("id_jenis_imunisasi", $kode);
        $this->db->update("jenis_imunisasi", $data);
    }

    function delete($kode)
    {
        $this->db->where("id_jenis_imunisasi", $kode);
        $this->db->delete("jenis_imunisasi");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */