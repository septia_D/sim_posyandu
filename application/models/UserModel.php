<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

    function insert($data)
    {
        $this->db->insert('user', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('username', $kode);
        return $this->db->get("user")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("username", $kode);
        $this->db->update("user", $data);
    }

    function delete($kode)
    {
        $this->db->where("username", $kode);
        $this->db->delete("user");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */