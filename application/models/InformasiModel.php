<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InformasiModel extends CI_Model {
    
    function get_all()
    {
        $this->db->order_by("tgl_post", "DESC");
        $query = $this->db->get("informasi");
        return $query->result_array();
    }

    function insert($data)
    {
        $this->db->insert('informasi', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('id_informasi', $kode);
        return $this->db->get("informasi")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("id_informasi", $kode);
        $this->db->update("informasi", $data);
    }

    function delete($kode)
    {
        $this->db->where("id_informasi", $kode);
        $this->db->delete("informasi");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */