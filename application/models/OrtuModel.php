<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OrtuModel extends CI_Model
{

    function get_all()
    {
        $query = $this->db->get("orang_tua");
        return $query->result_array();
    }

    function insert($data)
    {
        $this->db->insert('orang_tua', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('id_orang_tua', $kode);
        return $this->db->get("orang_tua")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("id_orang_tua", $kode);
        $this->db->update("orang_tua", $data);
    }

    function delete($kode)
    {
        $this->db->where("id_orang_tua", $kode);
        $this->db->delete("orang_tua");
    }

    function insert_ortu_bayi($data)
    {
        $this->db->insert('ortu_bayi', $data);
    }

    function delete_ortu_bayi($kode)
    {
        $this->db->where("id_ortu_bayi", $kode);
        $this->db->delete("ortu_bayi");
    }

    function delete_ortu_bayi_id($id)
    {
        $this->db->where("id_orang_tua", $id);
        $this->db->delete("ortu_bayi");
    }

    function get_ortu_bayi($kode)
    {
        $this->db->where('id_ortu_bayi', $kode);
        return $this->db->get("ortu_bayi")->row();
    }

    function get_ortu_bayi_id($id_orang_tua)
    {
        $this->db->where('id_orang_tua', $id_orang_tua);
        return $this->db->get("ortu_bayi")->row();
    }

    function ambilDataBayi($username)
    {
        $this->db->select('*');
        $this->db->from('orang_tua AS a');
        $this->db->join('ortu_bayi AS b', 'a.id_orang_tua=b.id_orang_tua');
        $this->db->join('balita AS c', 'b.nib=c.nib');

        $this->db->where('username', $username);
        if (!empty($this->session->nib)) {
            $this->db->where('c.nib', $this->session->nib);
        }
        return $this->db->get()->row();
    }
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */