<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalModel extends CI_Model {
    
    function get_all($start_format=null, $end_format=null)
    {
        if(!empty($start_format) && !empty($end_format)) {
            $this->db->where('tgl_jadwal >=', $start_format);
            $this->db->where('tgl_jadwal <=', $end_format);
        }
        $query = $this->db->get("jadwal_pemeriksaan");
        return $query->result_array();
    }

    function insert($data)
    {
        $this->db->insert('jadwal_pemeriksaan', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('id_jadwal_pemeriksaan', $kode);
        return $this->db->get("jadwal_pemeriksaan")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("id_jadwal_pemeriksaan", $kode);
        $this->db->update("jadwal_pemeriksaan", $data);
    }

    function delete($kode)
    {
        $this->db->where("id_jadwal_pemeriksaan", $kode);
        $this->db->delete("jadwal_pemeriksaan");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */