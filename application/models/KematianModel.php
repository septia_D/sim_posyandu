<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KematianModel extends CI_Model {
    
    function get_all()
    {
        $this->db->join("balita", "kematian.nib=balita.nib");
        $query = $this->db->get("kematian");
        return $query->result_array();
    }

    function insert($data)
    {
        $this->db->insert('kematian', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('id_kematian', $kode);
        return $this->db->get("kematian")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("id_kematian", $kode);
        $this->db->update("kematian", $data);
    }

    function delete($kode)
    {
        $this->db->where("id_kematian", $kode);
        $this->db->delete("kematian");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */