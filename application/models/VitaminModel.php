<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VitaminModel extends CI_Model {
    
    function get_all()
    {
        $query = $this->db->get("jenis_vitamin");
        return $query->result_array();
    }

    function insert($data)
    {
        $this->db->insert('jenis_vitamin', $data);
    }

    function get_by_id($kode)
    {
        $this->db->where('id_jenis_vitamin', $kode);
        return $this->db->get("jenis_vitamin")->row();
    }

    function update($kode, $data)
    {
        $this->db->where("id_jenis_vitamin", $kode);
        $this->db->update("jenis_vitamin", $data);
    }

    function delete($kode)
    {
        $this->db->where("id_jenis_vitamin", $kode);
        $this->db->delete("jenis_vitamin");
    }

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */