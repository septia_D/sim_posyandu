<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4"><?= $button ?> Jadwal Pemeriksaan</h4>
    <hr>

    <form method="POST" action="<?= $action; ?>">
        <div class="form-group row">
            <input class="form-control" type="hidden" name="id_jadwal_pemeriksaan" value="<?= $id_jadwal_pemeriksaan ?>">
            <label class="col-sm-12 col-md-2 col-form-label">Tanggal</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Tanggal" type="date" name="tgl_jadwal" value="<?= $tgl_jadwal ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Jam Mulai</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Jam Mulai" type="time" name="jam_mulai" value="<?= $jam_mulai ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Jam Selesai</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Jam Selesai" type="time" name="jam_selese" value="<?= $jam_selese ?>" required>
            </div>
        </div>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"><?= $button ?></button>
                <a type="button" href="<?= site_url('controllerJadwal'); ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
</div>