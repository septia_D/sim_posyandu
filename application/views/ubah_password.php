<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4">Form Ubah Password</h4>
    <hr>

    <form method="POST" action="<?= site_url('ControllerLogin/ubah_password_action') ?>">
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Password Baru</label>
            <div class="col-sm-12 col-md-10">
                <input type="password" name="password" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
</div>