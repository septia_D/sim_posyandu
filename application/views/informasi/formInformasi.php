<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4"><?= $button ?> Informasi</h4>
    <hr>

    <form method="POST" action="<?= $action; ?>" enctype="multipart/form-data">
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tanggal</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Nama" type="text" name="tgl_post" value="<?= ($tgl_post) ? $tgl_post : date('d-m-Y H:i:s') ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <input class="form-control" type="hidden" name="id_informasi" value="<?= $id_informasi ?>">
            <label class="col-sm-12 col-md-2 col-form-label">Judul</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Judul" type="text" name="judul" value="<?= $judul ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Isi</label>
            <div class="col-sm-12 col-md-10">
                <textarea name="isi" id="isi" class="form-control summernote" cols="30" rows="10" required><?= $isi ?></textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="photo">Foto</label>
                <div class="col-sm-5">
                    <?php
                    if ($foto == "") {
                        echo "<p class='help-block'>Silahkan tekan choose file </p>";
                    } else {
                    ?>
                        <div>
                            <img src="<?php echo base_url() ?>images/informasi/<?= $foto; ?>" width="50%">
                        </div><br />
                    <?php
                    }
                    ?>
                    <input type="file" name="foto" id="foto">
                </div>
            </div>
        </div>


        <div class="form-group row text-center">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"><?= $button ?></button>
                <a type="button" href="<?= site_url('controllerInformasi'); ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
</div>