<!-- Simple Datatable start -->
<div class="card-box mb-30">
    <div class="pd-20">
        <h4 class="text-blue h4">Data Balita dan Pemeriksaan</h4>
        <hr>
        <a href="<?= site_url('ControllerBalita/insert') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
    </div>
    <div class="pb-20">
        <div class="table-responsive">
            <table class="data-table table stripe hover nowrap" id="myTable">
                <thead>
                    <tr>
                        <!-- <th class="table-plus datatable-nosort" style="text-align: center;">No</th> -->
                        <th style="text-align: center;">NIB</th>
                        <th style="text-align: center;">Nama Balita</th>
                        <th style="text-align: center;">Tgl. Lahir</th>
                        <th width="10%" style="text-align: center;">Jenis Kelamin</th>
                        <th style="text-align: center;">Ibu</th>
                        <!-- <th>Ayah</th> -->
                        <!-- <th>Alamat</th> -->
                        <th class="datatable-nosort" style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($balita as $value) : ?>
                        <tr>
                            <!-- <td style="text-align: center;" width="5%"><?= $no++; ?></td> -->
                            <td style="text-align: center;"><?= $value['nib'] ?></td>
                            <td><?= $value['nama_balita'] ?></td>
                            <td style="text-align: center;">
                                <?= date('d-m-Y', strtotime($value['tgl_lahir'])) ?><br>
                                <?= hitung_umur($value['tgl_lahir'])->y . " Tahun " . hitung_umur($value['tgl_lahir'])->m . " Bulan " . hitung_umur($value['tgl_lahir'])->d . " Hari"; ?>
                            </td>
                            <td>
                                <?php
                                if ($value['jenis_kelamin'] == "L") {
                                    $kelamin = "Laki-Laki";
                                } else if ($value['jenis_kelamin'] == "P") {
                                    $kelamin = "Perempuan";
                                }
                                ?>
                                <?= $kelamin ?>
                            </td>
                            <td><?= $value['nama_ibu'] ?></td>
                            <td style="text-align: center;">
                                <?php if ($value['is_meninggal']) { ?>
                                    <span class="badge badge-pill badge-warning">Sudah Meninggal</span>
                                <?php } else { ?>
                                    <div class="dropdown">
                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                            <i class="dw dw-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                            <a class="dropdown-item" href="<?= site_url("ControllerBalita/view/" . $value['nib']); ?>"><i class="dw dw-eye"></i> Pemeriksaan</a>
                                            <a class="dropdown-item" href="<?= site_url("ControllerBalita/edit/" . $value['nib']); ?>"><i class="dw dw-edit2"></i> Edit</a>
                                            <a class="dropdown-item hapus" href="<?= site_url("ControllerBalita/delete/" . $value['nib']); ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Simple Datatable End -->