<div class="row">
    <div class="col-xl-4 mb-30">
        <div class="card-box height-100-p pd-20">
            <h2 class="h4 mb-20"><?= $balita->nama_balita ?></h2>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" width="47%">NIB</th>
                        <th>: <?= $balita->nib ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Tgl. Lahir</th>
                        <th>: <?= date('d-m-Y', strtotime($balita->tgl_lahir)) ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Umur</th>
                        <th>: <?= hitung_umur($balita->tgl_lahir)->y . " Tahun " . hitung_umur($balita->tgl_lahir)->m . " Bulan " . hitung_umur($balita->tgl_lahir)->d . " Hari" ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Jenis Kelamin</th>
                        <th>: <?= $balita->jenis_kelamin ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Nama Ibu</th>
                        <th>: <?= $balita->nama_ibu ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Nama Ayah</th>
                        <th>: <?= $balita->nama_ayah ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Alamat</th>
                        <th>: <?= $balita->alamat ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Panjang Badan</th>
                        <th>: <?= $balita->panjang_badan. " cm" ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Berat Lahir</th>
                        <th>: <?= $balita->berat_lahir. " kg" ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Lingkar Kepala</th>
                        <th>: <?= $balita->lingkar_kepala. " cm" ?></th>
                    </tr>
                    <tr>
                        <th scope="row">NIK Balita</th>
                        <th>: <?= $balita->nik_balita ?></th>
                    </tr>
                    <tr>
                        <th scope="row">No. KK</th>
                        <th>: <?= $balita->no_kk ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Anak Ke</th>
                        <th>: <?= $balita->anak_ke ?></th>
                    </tr>
                    <tr>
                        <th colspan="2"><a href="<?= site_url('controllerBalita') ?>" class="btn btn-danger btn-block">Kembali</a></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-xl-8 mb-30">
        <div class="card-box height-100-p pd-20">
            <h2 class="h4 mb-20">Pemeriksaan</h2>
            <div class="pt-10 pb-20">
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#bd-example-modal-lg" type="button"><i class="fa fa-plus"></i> Tambah</a>
            </div>
            <div class="table-responsive">
                <table class="data-table table stripe hover nowrap" id="myTable">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Kode</th>
                            <th style="text-align: center;">Umur</th>
                            <th style="text-align: center;">Tgl. Timbang</th>
                            <th style="text-align: center;">Berat Badan</th>
                            <th style="text-align: center;">Panjang Badan</th>
                            <th style="text-align: center;">Lingkar Perut</th>
                            <th style="text-align: center;">Imunisasi</th>
                            <th style="text-align: center;">Vitamin</th>
                            <th style="text-align: center;">Saran</th>
                            <th class="datatable-nosort" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pemeriksaan as $value) : ?>
                            <tr>
                                <!-- <td style="text-align: center;" width="5%"><?= $no++; ?></td> -->
                                <td style="text-align: center;"><?= $value['kode_pemeriksaan'] ?></td>
                                <td><?= $value['umur'] ?></td>
                                <td><?= date('d-m-Y', strtotime($value['tgl_timbang'])) ?></td>
                                <td><?= $value['berat_badan']. " kg" ?></td>
                                <td><?= $value['panjang_badan']. " cm" ?></td>
                                <td><?= $value['lingkar_perut']. " cm" ?></td>
                                <td><?= $value['nama_imunisasi'] ?></td>
                                <td><?= $value['nama_vitamin'] ?></td>
                                <td><?= $value['saran'] ?></td>
                                <td style="text-align: center;">
                                    <a class="btn" href="<?= site_url("ControllerBalita/edit_pemeriksaan/" . $value['kode_pemeriksaan']); ?>"><i class="dw dw-edit2"></i> Edit</a>
                                    <a class="btn hapus" href="<?= site_url("ControllerBalita/delete_pemeriksaan/" . $value['kode_pemeriksaan'] . "/" . $value['nib']); ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Tambah Pemeriksaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="<?= site_url('controllerBalita/insert_pemeriksaan') ?>">
                    <input class="form-control" type="hidden" name="nib" value="<?= $balita->nib ?>" required readonly>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Umur</label>
                        <div class="col-sm-12 col-md-10">
                            <input class="form-control" placeholder="" type="text" name="umur" value="<?= hitung_umur($balita->tgl_lahir)->y . " Tahun " . hitung_umur($balita->tgl_lahir)->m . " Bulan " . hitung_umur($balita->tgl_lahir)->d . " Hari" ?>" required readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Tgl. Timbang</label>
                        <div class="col-sm-12 col-md-10">
                            <input class="form-control" placeholder="" type="date" name="tgl_timbang" value="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Berat Badan</label>
                        <div class="col-sm-11 col-md-9">
                            <input class="form-control" placeholder="Berat Badan" type="number" name="berat_badan" value="" required step="0.01">
                        </div>
                        <div class="col-sm-1 col-md-1">
                            KG
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Panjang Badan</label>
                        <div class="col-sm-12 col-md-9">
                            <input class="form-control" placeholder="Panjang Badan" type="number" name="panjang_badan" value="" required step="0.01">
                        </div>
                        <div class="col-sm-12 col-md-1">
                            CM
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Lingkar Perut</label>
                        <div class="col-sm-12 col-md-9">
                            <input class="form-control" placeholder="Lingkar Perut" type="number" name="lingkar_perut" value="" required step="0.01">
                        </div>
                        <div class="col-sm-12 col-md-1">
                            CM
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Jenis Imunisasi</label>
                        <div class="col-sm-12 col-md-10">
                            <select name="id_jenis_imunisasi" id="" class="form-control" required>
                                <?php
                                $dt_jenis_imunisasi = $this->db->get("jenis_imunisasi")->result();
                                ?>
                                <option value="" disabled selected>--Pilih--</option>
                                <?php foreach ($dt_jenis_imunisasi as $key => $value) { ?>
                                    <option value="<?= $value->id_jenis_imunisasi ?>"><?= $value->nama_imunisasi ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Jenis Vitamin</label>
                        <div class="col-sm-12 col-md-10">
                            <select name="id_jenis_vitamin" id="" class="form-control" required>
                                <?php
                                $dt_jenis_vitamin = $this->db->get("jenis_vitamin")->result();
                                ?>
                                <option value="" disabled selected>--Pilih--</option>
                                <?php foreach ($dt_jenis_vitamin as $key => $value) { ?>
                                    <option value="<?= $value->id_jenis_vitamin ?>"><?= $value->nama_vitamin ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-12 col-md-2 col-form-label">Saran</label>
                        <div class="col-sm-12 col-md-10">
                            <textarea class="form-control" name="saran" style="height: 120%" placeholder="Saran" required></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>