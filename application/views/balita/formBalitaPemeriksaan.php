<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4"><?= $button ?> Pemeriksaan</h4>
    <hr>

    <form method="POST" action="<?= $action; ?>">
        <input class="form-control" type="hidden" name="kode_pemeriksaan" value="<?= $kode_pemeriksaan ?>" required readonly>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">NIB</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="" type="text" name="nib" value="<?= $nib ?>" required readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Nama</label>
            <div class="col-sm-12 col-md-10">
                <?php
                $dt_balita = $this->db->get_where("balita", array("nib" => $nib))->row();
                ?>
                <input class="form-control" placeholder="" type="text" value="<?= $dt_balita->nama_balita ?>" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tgl. Timbang</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="" type="date" name="tgl_timbang" value="<?= $tgl_timbang ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Berat Badan</label>
            <div class="col-sm-11 col-md-9">
                <input class="form-control" placeholder="Berat Badan" type="number" name="berat_badan" value="<?= $berat_badan ?>" required step="0.01">
            </div>
            <div class="col-sm-1 col-md-1">
                KG
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Panjang Badan</label>
            <div class="col-sm-12 col-md-9">
                <input class="form-control" placeholder="Panjang Badan" type="number" name="panjang_badan" value="<?= $panjang_badan ?>" required step="0.01">
            </div>
            <div class="col-sm-12 col-md-1">
                CM
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Lingkar Perut</label>
            <div class="col-sm-12 col-md-9">
                <input class="form-control" placeholder="Lingkar Perut" type="number" name="lingkar_perut" value="<?= $lingkar_perut ?>" required step="0.01">
            </div>
            <div class="col-sm-12 col-md-1">
                CM
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Jenis Imunisasi</label>
            <div class="col-sm-12 col-md-10">
                <select name="id_jenis_imunisasi" id="" class="form-control" required>
                    <?php
                    $dt_jenis_imunisasi = $this->db->get("jenis_imunisasi")->result();
                    ?>
                    <option value="" disabled selected>--Pilih--</option>
                    <?php foreach ($dt_jenis_imunisasi as $key => $value) { ?>
                        <option value="<?= $value->id_jenis_imunisasi ?>" <?= ($value->id_jenis_imunisasi == $id_jenis_imunisasi) ? "selected" : "" ?>><?= $value->nama_imunisasi ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Jenis Vitamin</label>
            <div class="col-sm-12 col-md-10">
                <select name="id_jenis_vitamin" id="" class="form-control" required>
                    <?php
                    $dt_jenis_vitamin = $this->db->get("jenis_vitamin")->result();
                    ?>
                    <option value="" disabled selected>--Pilih--</option>
                    <?php foreach ($dt_jenis_vitamin as $key => $value) { ?>
                        <option value="<?= $value->id_jenis_vitamin ?>" <?= ($value->id_jenis_vitamin == $id_jenis_vitamin) ? "selected" : "" ?>><?= $value->nama_vitamin ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Saran</label>
            <div class="col-sm-12 col-md-10">
                <textarea class="form-control" name="saran" style="height: 120%" placeholder="Saran" required><?= $saran ?></textarea>
            </div>
        </div>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"><?= $button ?></button>
                <a type="button" href="<?= site_url("controllerBalita/view/") . $nib; ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
</div>