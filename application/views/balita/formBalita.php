<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4"><?= $button ?> Balita</h4>
    <hr>

    <form method="POST" action="<?= $action; ?>">
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">NIB</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" name="nib" value="<?= $nib ?>" required readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Balita</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Nama" type="text" name="nama_balita" value="<?= $nama_balita ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">NIK Balita</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="NIK Balita" type="text" name="nik_balita" value="<?= $nik_balita ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">No. KK</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="No. KK" type="text" name="no_kk" value="<?= $no_kk ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Anak Ke</label>
            <div class="col-sm-12 col-md-10">
                <select name="anak_ke" id="anak_ke" class="form-control" required>
                    <option value="" disabled selected>--Pilih--</option>
                    <?php
                    $dt_urut = [1, 2, 3, 4, 5];
                    ?>
                    <?php foreach ($dt_urut as $value) { ?>
                        <option value="<?= $value ?>" <?= ($value == $anak_ke) ? "selected" : "" ?>><?= $value ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tgl. Lahir</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="" type="date" name="tgl_lahir" value="<?= $tgl_lahir ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Jenis Kelamin</label>
            <div class="custom-control custom-radio mb-5">
                <input type="radio" id="laki-laki" name="jenis_kelamin" value="L" class="custom-control-input" <?= ($jenis_kelamin == "L") ? "checked" : "" ?>>
                <label class="custom-control-label" for="laki-laki">Laki-Laki</label>
            </div>
            <div class="custom-control custom-radio ml-5">
                <input type="radio" id="perempuan" name="jenis_kelamin" value="P" class="custom-control-input" <?= ($jenis_kelamin == "P") ? "checked" : "" ?>>
                <label class="custom-control-label" for="perempuan">Perempuan</label>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Ibu</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Nama" type="text" name="nama_ibu" value="<?= $nama_ibu ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Ayah</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Nama" type="text" name="nama_ayah" value="<?= $nama_ayah ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Alamat</label>
            <div class="col-sm-12 col-md-10">
                <textarea class="form-control" name="alamat" rows="3" placeholder="Alamat" required><?= $alamat ?></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Panjang Badan</label>
            <div class="col-sm-12 col-md-9">
                <input class="form-control" placeholder="Panjang Badan" type="number" name="panjang_badan" value="<?= $panjang_badan ?>" required step="0.01">
            </div>
            <div class="col-sm-12 col-md-1">
                CM
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Berat Lahir</label>
            <div class="col-sm-12 col-md-9">
                <input class="form-control" placeholder="Berat Lahir" type="number" name="berat_lahir" value="<?= $berat_lahir ?>" required step="0.01">
            </div>
            <div class="col-sm-12 col-md-1">
                KG
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Lingkar Kepala</label>
            <div class="col-sm-12 col-md-9">
                <input class="form-control" placeholder="Lingkar Kepala" type="number" name="lingkar_kepala" value="<?= $lingkar_kepala ?>" required step="0.01">
            </div>
            <div class="col-sm-12 col-md-1">
                CM
            </div>
        </div>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"><?= $button ?></button>
                <a type="button" href="<?= site_url('controllerBalita'); ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
</div>