<!DOCTYPE html>
<html>

<head>
    <!-- Basic Page Info -->
    <meta charset="utf-8">
    <title>Posyandu Ceria</title>

    <!-- Site favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= site_url('theme/deskapp-master/') ?>vendors/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= site_url('theme/deskapp-master/') ?>vendors/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= site_url('theme/deskapp-master/') ?>vendors/images/favicon-16x16.png">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?= site_url('theme/deskapp-master/') ?>vendors/styles/core.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url('theme/deskapp-master/') ?>vendors/styles/icon-font.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url('theme/deskapp-master/') ?>vendors/styles/style.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-119386393-1');
    </script>
</head>

<body class="login-page">
    <div class="login-header box-shadow">
        <div class="container-fluid d-flex justify-content-between align-items-center">
            <div class="brand-logo">
                <a href="#">
                    <h3>Posyandu Ceria</h3>
                </a>
            </div>
        </div>
    </div>
    <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-lg-7">
                    <img src="<?= site_url('images/baby.svg') ?>" alt="">
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="login-box bg-white box-shadow border-radius-10">
                        <div class="login-title">
                            <h2 class="text-center text-primary">Login</h2>
                        </div>
                        <form action="<?= base_url('controllerLogin/cekStatusLogin'); ?>" method="POST">
                            <small class="text-danger"><?= form_error('username') ?></small>
                            <div class="input-group custom">
                                <input type="text" name="username" class="form-control form-control-lg" placeholder="Username">
                                <div class="input-group-append custom">
                                    <span class="input-group-text"><i class="icon-copy dw dw-user1"></i> </span>
                                </div>
                            </div>
                            <small class="text-danger"><?= form_error('password') ?></small>
                            <div class="input-group custom">
                                <input type="password" name="password" class="form-control form-control-lg" placeholder="**********">
                                <div class="input-group-append custom">
                                    <span class="input-group-text"><i class="dw dw-padlock1"></i> </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-group mb-0">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block text-white">Masuk</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- js -->
    <script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/core.js"></script>
    <script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/script.min.js"></script>
    <script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/process.js"></script>
    <script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/layout-settings.js"></script>
    <script src="<?= site_url(); ?>plugins/sweetalert/package/dist/sweetalert2.all.min.js"></script>
    <?php if (($this->session->flashdata('gagal')) != "") : ?>
        <script type="text/javascript">
            $(document).ready(function() {
                Swal.fire({
                    title: 'Gagal',
                    text: "<?= $this->session->flashdata('gagal') ?>",
                    icon: 'error',
                })
            });
        </script>
    <?php endif; ?>
</body>

</html>