<div class="col-md-12 col-sm-12 mb-30">
    <div class="pd-20 card-box height-100-p">
        <h4 class="mb-15 text-blue h4">Menu Kader</h4>
        <hr>
        <div class="btn-list pt-30">
            <h5>Data Master</h5>
            <a href="<?= site_url('controllerImunisasi') ?>" class="btn" data-bgcolor="#3b5998" data-color="#ffffff" style="width: 5.6cm;"><i class="icon-copy fa fa-medkit" aria-hidden="true"></i> Data Imunisasi</a>
            <a href="<?= site_url('controllerVitamin') ?>" class="btn" data-bgcolor="#3b5998" data-color="#ffffff" style="width: 5.6cm;"><i class="icon-copy fa fa-plus-square" aria-hidden="true"></i> Data Vitamin</a>
            <a href="<?= site_url('controllerOrtu') ?>" class="btn" data-bgcolor="#3b5998" data-color="#ffffff" style="width: 5.6cm;"><i class="icon-copy fa fa-user-plus" aria-hidden="true"></i> Data Orangtua</a>
            <a href="<?= site_url('controllerKematian') ?>" class="btn" data-bgcolor="#3b5998" data-color="#ffffff" style="width: 5.6cm;"><i class="icon-copy fa fa-user-times" aria-hidden="true"></i> Data Kematian</a>
        </div>

        <div class="btn-list pt-30">
            <h5>Pemeriksaan</h5>
            <a href="<?= site_url('controllerBalita') ?>" class="btn" data-bgcolor="#f46f30" data-color="#ffffff" style="width: 5.6cm;"><i class="fa fa-user"></i> Data Balita</a>
            <!-- <button type="button" class="btn" data-bgcolor="#f46f30" data-color="#ffffff"><i class="icon-copy fa fa-stethoscope" aria-hidden="true"></i> Data Pemeriksaan</button> -->
        </div>

        <div class="btn-list pt-30">
            <h5>Laporan</h5>
            <a href="<?= site_url('controllerLaporan') ?>" class="btn" data-bgcolor="#00b489" data-color="#ffffff" style="width: 5.6cm;"><i class="icon-copy fa fa-book" aria-hidden="true"></i> Laporan</a>
        </div>

        <div class="btn-list pt-30">
            <h5>Setting</h5>
            <a href="<?= site_url('controllerJadwal') ?>" class="btn" data-bgcolor="#3d464d" data-color="#ffffff" style="width: 5.6cm;"><i class="icon-copy fa fa-calendar" aria-hidden="true"></i> Jadwal Pemeriksaan</a>
            <a href="<?= site_url('controllerInformasi') ?>" class="btn" data-bgcolor="#3d464d" data-color="#ffffff" style="width: 5.6cm;"><i class="icon-copy fa fa-bullhorn" aria-hidden="true"></i> Informasi</a>
        </div>
    </div>
</div>