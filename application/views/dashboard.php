<?php
$get_data_user = $this->db->get_where("user", array("username" => $this->session->session_login["username"]))->row();
?>
<div class="pd-ltr-20">
    <div class="card-box pd-20 height-100-p mb-30">
        <div class="row align-items-center">
            <div class="col-md-4">
                <img src="<?= site_url('images/nurse.svg') ?>" alt="">
            </div>
            <div class="col-md-8">
                <h4 class="font-20 weight-500 mb-10 text-capitalize">
                    Selamat Datang <div class="weight-600 font-30 text-blue"><?= $get_data_user->nama ?> !</div>
                </h4>
                <p class="font-18 max-width-600">Aplikasi SIM Posyandu ini merupakan .....</p>
            </div>
        </div>
    </div>
</div>

<?php if ($get_data_user->role != 'admin') { ?>

    <div class="blog-wrap">
        <div class="container">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="title">
                            <h4>Informasi</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="blog-list">
                        <ul>
                            <?php foreach ($informasi as $key => $value) { ?>
                                <li>
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 col-md-12 col-sm-12">
                                            <div class="blog-img">
                                                <img src="<?= site_url('images/informasi/'. $value["foto"]) ?>" alt="" class="" style="width: 100%;">
                                            </div>
                                    </div>
                                    <div class="col-lg-8 col-md-12 col-sm-12">
                                        <div class="blog-caption">
                                            <h4><a href="#"><?= $value["judul"] ?></a></h4>
                                            <div class="blog-by">
                                                <p><?= $value["isi"] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>