</div>
<!-- js -->
<script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/core.js"></script>
<script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/script.min.js"></script>
<script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/process.js"></script>
<script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/layout-settings.js"></script>
<script src="<?= site_url(); ?>plugins/sweetalert/package/dist/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= site_url('theme/deskapp-master/') ?>src/plugins/highcharts-6.0.7/code/highcharts.js"></script>
<!-- <script src="https://code.highcharts.com/highcharts-3d.js"></script> -->
<script src="<?= site_url('theme/deskapp-master/') ?>src/plugins/highcharts-6.0.7/code/highcharts-more.js"></script>
<script src="<?= site_url('theme/deskapp-master/') ?>src/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<!-- <script src="<?= site_url('theme/deskapp-master/') ?>vendors/scripts/highchart-setting.js"></script> -->

</body>
<?php if (($this->session->flashdata('sukses')) != "") : ?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Berhasil',
                text: "<?= $this->session->flashdata('sukses') ?>",
                icon: 'success',
                showConfirmButton: false,
                timer: 1500
            })
        });
    </script>
<?php endif; ?>
<?php if (($this->session->flashdata('gagal')) != "") : ?>
    <script type="text/javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Gagal',
                text: "<?= $this->session->flashdata('gagal') ?>",
                icon: 'error',
            })
        });
    </script>
<?php endif; ?>

<script>
    $('.hapus').on('click', function(e) {

        event.preventDefault();
        const href = $(this).attr('href');

        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data akan dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then((result) => {
            if (result.value) {
                document.location.href = href;
            }
        })
    });
</script>

<script>
    $(document).ready(function() {
        $('.summernote').summernote({
            placeholder: 'Silahkan diisi',
            tabsize: 2,
            height: 200
        });
        
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>

</html>