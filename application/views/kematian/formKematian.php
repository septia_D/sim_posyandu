<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4"><?= $button ?> Kematian</h4>
    <hr>

    <form method="POST" action="<?= $action; ?>">
        <div class="form-group row">
            <input class="form-control" type="hidden" name="id_kematian" value="<?= $id_kematian ?>">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Balita</label>
            <div class="col-sm-12 col-md-10">
                <select name="nib" id="nib" class="form-control" required>
                    <option value="" disabled selected>--Pilih--</option>
                    <?php
                    $dt_balita = $this->db->get("balita")->result();
                    ?>
                    <?php foreach ($dt_balita as $key => $value) { ?>
                        <option value="<?= $value->nib ?>" <?= ($value->nib == $nib) ? "selected" : "" ?>><?= $value->nib ?> - <?= $value->nama_balita ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Ibu</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" value="" id="nama_ibu" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tgl. Lahir</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" id="tgl_lahir" value="" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tgl. Kematian</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="" type="date" name="tgl_kematian" value="<?= $tgl_kematian ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Keterangan</label>
            <div class="col-sm-12 col-md-10">
                <textarea class="form-control" name="keterangan" rows="3" placeholder="Keterangan" required><?= $keterangan ?></textarea>
            </div>
        </div>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"><?= $button ?></button>
                <a type="button" href="<?= site_url('ControllerKematian'); ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
</div>
<script src="<?php echo base_url(); ?>plugins/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var nib = $('#nib').val();
        
        get_dtBalita(nib);
        function get_dtBalita(nib) {
            $.ajax({ //create an ajax request to display.php
                type: "POST",
                url: "<?= site_url('ControllerKematian/get_dtBalita') ?>",
                data: {
                    nib: nib
                },
                dataType: "JSON", //expect html to be returned                
                success: function(response) {
                    $('#nama_ibu').val(response.nama_ibu);
                    $('#tgl_lahir').val(response.tgl_lahir);
                }

            });
        }

        $('#nib').change(function(e) {
            var nib = $('#nib').val();

            get_dtBalita(nib);
        });
    });
</script>