<!-- Simple Datatable start -->
<div class="card-box mb-30">
    <div class="pd-20">
        <h4 class="text-blue h4">Data Kematian</h4>
        <hr>
        <a href="<?= site_url('ControllerKematian/insert') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
    </div>
    <div class="pb-20">
        <div class="table-responsive">
            <table class="data-table table stripe hover nowrap" id="myTable">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort" style="text-align: center;">No</th>
                        <th>Nama</th>
                        <th>NIB</th>
                        <th>Nama Ibu</th>
                        <th>Tgl. Lahir</th>
                        <th>Tgl. Kematian</th>
                        <th>Keterangan</th>
                        <th class="datatable-nosort" style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($kematian as $value) : ?>
                        <tr>
                            <td style="text-align: center;" width="5%"><?= $no++; ?></td>
                            <td><?= $value['nama_balita'] ?></td>
                            <td><?= $value['nib'] ?></td>
                            <td><?= $value['nama_ibu'] ?></td>
                            <td><?= date('d-m-Y', strtotime($value['tgl_lahir'])) ?></td>
                            <td><?= date('d-m-Y', strtotime($value['tgl_kematian'])) ?></td>
                            <td><?= $value['keterangan'] ?></td>
                            <td style="text-align: center;">
                                <div class="dropdown">
                                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="dw dw-more"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                        <a class="dropdown-item" href="<?= site_url("ControllerKematian/edit/" . $value['id_kematian']); ?>"><i class="dw dw-edit2"></i> Edit</a>
                                        <a class="dropdown-item hapus" href="<?= site_url("ControllerKematian/delete/" . $value['id_kematian']); ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Simple Datatable End -->