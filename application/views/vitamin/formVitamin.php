<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4"><?= $button ?> Jenis Vitamin</h4>
    <hr>

    <form method="POST" action="<?= $action; ?>">
        <div class="form-group row">
            <input class="form-control" type="hidden" name="id_jenis_vitamin" value="<?= $id_jenis_vitamin ?>">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Vitamin</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Nama" type="text" name="nama_vitamin" value="<?= $nama_vitamin ?>" required>
            </div>
        </div>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"><?= $button ?></button>
                <a type="button" href="<?= site_url('ControllerVitamin'); ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
</div>