<?php if ($jml_balita > 1) { ?>
    <div class="row">
        <div class="col-xl-12 mb-30">
            <div class="card-box height-100-p pd-20">
                <select name="nib" id="nib" class="form-control" required>
                    <option value="" disabled selected>--Pilih Balita--</option>
                    <?php
                    $dt_balita = $this->db->query("SELECT * FROM balita JOIN ortu_bayi ON balita.nib=ortu_bayi.nib JOIN orang_tua ON ortu_bayi.id_orang_tua=orang_tua.id_orang_tua WHERE orang_tua.id_orang_tua='$balita->id_orang_tua'")->result();
                    // print_r($dt_balita);
                    // die;
                    ?>
                    <?php foreach ($dt_balita as $key => $value) { ?>
                        <option value="<?= $value->nib ?>"><?= $value->nib ?> - <?= $value->nama_balita ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
<?php } ?>
<div class="row">
    <div class="col-xl-4 mb-30">
        <div class="card-box height-100-p pd-20">
            <h2 class="h4 mb-20"><?= $balita->nama_balita ?></h2>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" width="47%">NIB</th>
                        <th>: <?= $balita->nib ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Tgl. Lahir</th>
                        <th>: <?= date('d-m-Y', strtotime($balita->tgl_lahir)) ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Jenis Kelamin</th>
                        <th>: <?= $balita->jenis_kelamin ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Nama Ibu</th>
                        <th>: <?= $balita->nama_ibu ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Nama Ayah</th>
                        <th>: <?= $balita->nama_ayah ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Alamat</th>
                        <th>: <?= $balita->alamat ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Panjang Badan</th>
                        <th>: <?= $balita->panjang_badan ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Berat Lahir</th>
                        <th>: <?= $balita->berat_lahir ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Lingkar Kepala</th>
                        <th>: <?= $balita->lingkar_kepala ?></th>
                    </tr>
                    <tr>
                        <th scope="row">NIK Balita</th>
                        <th>: <?= $balita->nik_balita ?></th>
                    </tr>
                    <tr>
                        <th scope="row">No. KK</th>
                        <th>: <?= $balita->no_kk ?></th>
                    </tr>
                    <tr>
                        <th scope="row">Anak Ke</th>
                        <th>: <?= $balita->anak_ke ?></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php
    $dt_pemeriksaan = $this->db->get_where("pemeriksaan", array("nib" => $balita->nib))->result();
    ?>

    <div class="col-xl-8 mb-30">
        <div class="card-box height-100-p pd-20">
            <h2 class="h4 mb-20">Pemeriksaan</h2>
            <hr>
            <div id="chart1"></div>
            <hr>
            <table class="table table-bordered table-resposive">
                <thead>
                    <tr>
                        <th>Tgl. Timbang</th>
                        <th>Jenis Imnunisasi</th>
                        <th>Jenis Vitamin</th>
                        <th>Saran</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dt_pemeriksaan as $key => $val_pemeriksaan) { ?>
                        <tr>
                            <td>
                                <?= date('d-m-Y', strtotime($val_pemeriksaan->tgl_timbang)) ?><br>
                                Imunisasi Ke <?= ++$key ?>
                            </td>
                            <td>
                                <?php
                                $dt_jenis_imunisasi = $this->db->get_where("jenis_imunisasi", array("id_jenis_imunisasi" => $val_pemeriksaan->id_jenis_imunisasi))->row();
                                ?>
                                <?= $dt_jenis_imunisasi->nama_imunisasi ?>
                            </td>
                            <td>
                                <?php
                                $dt_jenis_vitamin = $this->db->get_where("jenis_vitamin", array("id_jenis_vitamin" => $val_pemeriksaan->id_jenis_vitamin))->row();
                                ?>
                                <?= $dt_jenis_vitamin->nama_vitamin ?>
                            </td>
                            <td><?= $val_pemeriksaan->saran ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>plugins/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // chart 1
        Highcharts.chart('chart1', {
            title: {
                text: 'Grafik Perkembangan Balita'
            },
            subtitle: {
                text: '<?= $balita->nama_balita ?>'
            },
            yAxis: {
                title: {
                    text: 'cm / kg'
                },
                labels: {
                    formatter: function() {
                        return this.value + '';
                    }
                }
            },
            xAxis: {
                title: {
                    text: 'Imunisasi'
                },
                categories: [
                    <?php
                    foreach ($dt_pemeriksaan as $key => $val_cate) {
                        echo ++$key . ", ";
                    }
                    ?>
                ],
                gridLineWidth: 0,
                gridLineColor: '#e5f2fd',
                minorTickWidth: 0
            },
            chart: {
                type: 'spline',
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    // pointStart: 2010
                },
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Berat Badan',
                data: [
                    <?php
                    foreach ($dt_pemeriksaan as $key => $val_bb) {
                        echo $val_bb->berat_badan . ",";
                    }
                    ?>
                ]
            }, {
                name: 'Panjang Badan',
                data: [
                    <?php
                    foreach ($dt_pemeriksaan as $key => $val_bb) {
                        echo $val_bb->panjang_badan . ",";
                    }
                    ?>
                ]
            }, {
                name: 'Lingkar Perut',
                data: [
                    <?php
                    foreach ($dt_pemeriksaan as $key => $val_bb) {
                        echo $val_bb->lingkar_perut . ",";
                    }
                    ?>
                ]
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    }
                }]
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#nib').change(function(e) {
            var nib = $(this).val();

            $.ajax({
                type: "POST",
                url: "<?= site_url('ControllerDepanOrtu/session_filter_bayi') ?>",
                data: {
                    nib: nib
                },
                cache: false,
                dataType: "JSON",
                success: function(data) {
                    location.reload();
                }
            });
        })
    });
</script>