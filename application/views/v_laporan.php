<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
    <div class="pd-20 card-box">
        <h5 class="h4 text-blue mb-20">Laporan</h5>
        <div class="tab">
            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active text-blue" data-toggle="tab" href="#balita" role="tab" aria-selected="true">Balita </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-blue" data-toggle="tab" href="#pemeriksaan" role="tab" aria-selected="false">Pemeriksaan </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-blue" data-toggle="tab" href="#kematian" role="tab" aria-selected="false">Kematian </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="balita" role="tabpanel">
                    <div class="pd-20">
                        <a href="<?= site_url('controllerLaporan/cetak_balita') ?>" class="btn" data-bgcolor="#00b489" data-color="#ffffff"><i class="icon-copy fa fa-print" aria-hidden="true"></i> Cetak Data Balita</a>
                    </div>
                </div>
                <div class="tab-pane fade" id="pemeriksaan" role="tabpanel">
                    <div class="pd-20">
                        <select name="nib" id="nib" class="form-control">
                            <option value="" disabled selected>--Pilih Balita--</option>
                            <?php
                            $dt_balita = $this->db->get("balita")->result();
                            ?>
                            <?php foreach ($dt_balita as $key => $value) { ?>
                                <option value="<?= $value->nib ?>"><?= $value->nib ?> - <?= $value->nama_balita ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="pd-20">
                        <a href="<?= site_url('controllerLaporan/cetak_pemeriksaan') ?>" class="btn" data-bgcolor="#00b489" data-color="#ffffff" id="cetak_pemeriksaan"><i class="icon-copy fa fa-print" aria-hidden="true"></i> Cetak Data Pemeriksaan</a>
                    </div>
                </div>
                <div class="tab-pane fade" id="kematian" role="tabpanel">
                    <div class="pd-20">
                        <a href="<?= site_url('controllerLaporan/cetak_kematian') ?>" class="btn" data-bgcolor="#00b489" data-color="#ffffff"><i class="icon-copy fa fa-print" aria-hidden="true"></i> Cetak Data Kematian</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>plugins/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#cetak_pemeriksaan').click(function(e) {
            e.preventDefault();

            var nib = $('#nib').val();
            const href = $(this).attr('href');

            if (nib == null || nib == '') {
                Swal.fire({
                    title: 'Gagal',
                    text: "Silahkan pilih balita terlebih dahulu",
                    icon: 'error',
                })
            } else {
                location.href = href+ "/" + nib;
            }
        });
    });
</script>