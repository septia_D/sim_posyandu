<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4"><?= $button ?> Orang Tua</h4>
    <hr>

    <form method="POST" action="<?= $action; ?>">
        <div class="form-group row">
            <input class="form-control" type="hidden" name="id_orang_tua" value="<?= $id_orang_tua ?>">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Wali</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Nama" type="text" name="nama" value="<?= $nama ?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Username</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Username" type="text" name="username" value="<?= $username ?>" required>
            </div>
        </div>
        <?php if($button == 'Tambah') { ?>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Password</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" placeholder="Password" type="password" name="password" value="" required>
            </div>
        </div>
        <?php } ?>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"><?= $button ?></button>
                <a type="button" href="<?= site_url('controllerOrtu'); ?>" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </form>
</div>