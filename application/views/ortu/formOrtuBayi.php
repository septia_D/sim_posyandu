<div class="pd-20 card-box mb-30">
    <h4 class="text-blue h4">Tambah Bayi</h4>
    <hr>

    <form method="POST" action="<?= site_url('ControllerOrtu/tambah_balita') ?>">
        <input class="form-control" type="hidden" name="id_ortu" value="<?= $id_ortu ?>" id="id_ortu" required readonly>

        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Bayi</label>
            <div class="col-sm-12 col-md-10">
                <select name="nib" id="nib" class="form-control" required>
                    <option value="" disabled selected>--Pilih--</option>
                    <?php
                    $dt_balita = $this->db->get("balita")->result();
                    ?>
                    <?php foreach ($dt_balita as $key => $value) { ?>
                        <option value="<?= $value->nib ?>"><?= $value->nib ?> - <?= $value->nama_balita ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="<?= site_url('controllerOrtu') ?>" class="btn btn-danger">Kembali</a>
    </form>
</div>