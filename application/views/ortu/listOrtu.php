<!-- Simple Datatable start -->
<div class="card-box mb-30">
    <div class="pd-20">
        <h4 class="text-blue h4">Data Orang Tua</h4>
        <hr>
        <a href="<?= site_url('ControllerOrtu/insert') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
    </div>
    <div class="pb-20">
        <table class="data-table table stripe hover nowrap" id="myTable">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort" style="text-align: center;">No</th>
                    <th>Nama Wali</th>
                    <th>Username</th>
                    <th>Balita</th>
                    <th class="datatable-nosort" style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; ?>
                <?php foreach ($ortu as $value) : ?>
                    <tr>
                        <td style="text-align: center;" width="5%"><?= $no++; ?></td>
                        <td><?= $value['nama'] ?></td>
                        <td><?= $value['username'] ?></td>
                        <td>
                            <?php
                            $ortu = $value['id_orang_tua'];
                            $ambil_bayi = $this->db->query("SELECT * FROM ortu_bayi JOIN balita ON ortu_bayi.nib=balita.nib WHERE id_orang_tua='$ortu'")->result();

                            foreach ($ambil_bayi as $key => $val) {
                            ?>
                                <p><?= $val->nama_balita. " - ". $val->nib ?> <a href="<?= site_url("controllerOrtu/hapus_ortu_bayi/" . $val->id_ortu_bayi); ?>"><i class="icon-copy fa fa-times-rectangle" aria-hidden="true"></i></a></p>
                            <?php } ?>
                            <a href="<?= site_url('controllerOrtu/tambah_bayi_form/'. $value['id_orang_tua']) ?>" id="tambah_bayi" style="color: brown;"><i class="fa fa-user"></i> Tambah Balita</a>
                        </td>
                        <td style="text-align: center;">
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="<?= site_url("controllerOrtu/edit/" . $value['id_orang_tua']); ?>"><i class="dw dw-edit2"></i> Edit</a>
                                    <a class="dropdown-item hapus" href="<?= site_url("controllerOrtu/delete/" . $value['id_orang_tua']); ?>"><i class="dw dw-delete-3"></i> Delete</a>
                                </div>
                            </div>

                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
