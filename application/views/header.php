<!DOCTYPE html>
<html>

<head>
    <!-- Basic Page Info -->
    <meta charset="utf-8">
    <title>Posyandu Ceria</title>

    <!-- Site favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= site_url('theme/deskapp-master/') ?>vendors/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= site_url('theme/deskapp-master/') ?>vendors/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= site_url('theme/deskapp-master/') ?>vendors/images/favicon-16x16.png">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?= site_url('theme/deskapp-master/') ?>vendors/styles/core.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url('theme/deskapp-master/') ?>vendors/styles/icon-font.min.css">
    <link rel="stylesheet" type="text/css" href="<?= site_url('theme/deskapp-master/') ?>vendors/styles/style.css">
    <link href="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?= site_url('theme/deskapp-master/') ?>src/plugins/fullcalendar/fullcalendar.css">
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-119386393-1');
    </script>
</head>

<body>
    <!-- <div class="pre-loader">
        <div class="pre-loader-box">
            <div class='loader-progress' id="progress_div">
                <div class='bar' id='bar1'></div>
            </div>
            <div class='percent' id='percent1'>0%</div>
            <div class="loading-text">
                Loading...
            </div>
        </div>
    </div> -->

    <div class="header">
        <div class="header-left">
            <div class="menu-icon dw dw-menu"></div>
        </div>
        <div class="header-right">
            <div class="user-info-dropdown">
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                        <span class="user-icon">
                            <img src="<?= site_url('images/user.svg') ?>" alt="">
                        </span>
                        <?php
                        $get_data_user = $this->db->get_where("user", array("username" => $this->session->session_login["username"]))->row();
                        ?>
                        <span class="user-name"><?= $get_data_user->nama ?></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                        <a class="dropdown-item" href="<?= site_url('ControllerLogin/logout') ?>"><i class="dw dw-logout"></i> Log Out</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="left-side-bar">
        <div class="brand-logo mb-10 text-center">
            <h4 class="text-white text-center pt-15">Posyandu Ceria </h4>
            <h6 class="text-white text-center">Mardi Putra 5</h6>
            <div class="close-sidebar" data-toggle="left-sidebar-close">
                <i class="ion-close-round"></i>
            </div>
        </div>
        <div class="menu-block customscroll">
            <div class="sidebar-menu">
                <ul id="accordion-menu">
                    <li>
                        <a href="<?= site_url('controllerDashboard') ?>" class="dropdown-toggle no-arrow">
                            <span class="micon dw dw-house-1"></span><span class="mtext">Home</span>
                        </a>
                    </li>
                    <?php if ($get_data_user->role == 'admin') { ?>
                        <li>
                            <a href="<?= site_url('controllerKader') ?>" class="dropdown-toggle no-arrow">
                                <span class="micon dw dw-user"></span><span class="mtext">Menu Kader</span>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li>
                            <a href="<?= site_url('controllerDepanOrtu') ?>" class="dropdown-toggle no-arrow">
                                <span class="micon dw dw-user"></span><span class="mtext">Menu Orangtua</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= site_url('controllerJadwal/lihat_jadwal') ?>" class="dropdown-toggle no-arrow">
                                <span class="micon dw dw-calendar1"></span><span class="mtext">Jadwal Pemeriksaan</span>
                            </a>
                        </li>
                    <?php } ?>
                    <li>
                        <a href="<?= site_url('controllerLogin/ubahPassword') ?>" class="dropdown-toggle no-arrow"> <span class="micon dw dw-key3"></span><span class="mtext">Ubah Password</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-menu-overlay"></div>

    <div class="main-container">